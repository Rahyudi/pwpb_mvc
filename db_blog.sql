-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 30, 2023 at 07:15 AM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `subtitle`, `image`, `content`) VALUES
(163, 'Top 10 Mahluk Hidup Tercomel', 'Top 10 Mahluk Hidup Tercomel Di Bumi', '1697432549download.jpg', '1.Kamu'),
(164, 'Top 10 Cowok Ghibli', 'Top 10 Cowok Ghibli Di Dunia', '1697432721pexels-aline-viana-prado-2465877.jpg', '1.Vigzama');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('user','admin') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `password`, `role`) VALUES
(1, 'user@gmail.com', 'User1', '1234567', 'user'),
(2, 'laba@gmail.com', 'dekadi', '$2y$10$rvM.Bs7PsWKRVEp2Fr8itOXKT/3xQdo3Kzh4NrssIiISZl5EpG2GO', 'admin'),
(3, 'vignan@gmail.com', 'vignan', 'password', 'user'),
(4, 'yudiii@gmail.com', 'yudii', '$2y$10$8uesEUcqHWNDe1BVc1XmmeBc4Sjw6oDwJYlGBoGhkMsfq7TG6m6MK', 'user'),
(5, 'yudii@gmail.com', 'yudi', '$2y$10$t84Es2mSLrFMm6sQwrvvsOmRzSl1cRP6B.Z.qu7UBGcTar0g7B79y', 'user'),
(6, 'adi@gmail.com', 'yudi', '$2y$10$zgHv5P3mZZUCUHcVurATLeVuqnp4etUQ7fkDzhOB1p9wpAl/pqxSe', 'user'),
(7, 'didi@gmail.com', 'didi', '$2y$10$1kYsg8OsxMKYYzshgKeWbuqMVI9haM/nsqam7Z4d/jwYlGz.py056', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
