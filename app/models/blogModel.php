<?php

class blogModel
{
    private $tabel = 'blog';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllBlog()
    {
        $this->db->query('SELECT * FROM ' . $this->tabel);
        return $this->db->resultSet();
    }

    public function getAllBlogById($id)
    {
        $this->db->query('SELECT * FROM ' . $this->tabel . ' WHERE id=:id');
        $this->db->bind('id', $id);
        return $this->db->single();
    }

    // insert data

    public function newBlog($data)
    {
        $image = $_FILES['image']['name'];
        $imageTmpName = $_FILES['image']['tmp_name'];
        $allowedimg = array('jpg', 'jpeg', 'png');
        $extension = pathinfo($image, PATHINFO_EXTENSION);
        if (in_array($extension, $allowedimg)) {
            // Generate nama file unik dengan timestamp
            $uniqueFileName = time() . '' . $image;

            // Lokasi penyimpanan file yang akan diunggah
            $uploadPath = '../public/img/' . $uniqueFileName;

            // Memindahkan file yang diunggah ke lokasi penyimpanan
            if (move_uploaded_file($imageTmpName, $uploadPath)) {
                // File berhasil diunggah, sekarang masukkan informasi ke database
                $query = "INSERT INTO blog (title, subtitle, image, content) VALUES (:title, :subtitle, :image, :content)";
                $this->db->query($query);
                $this->db->bind('title', $data['title']);
                $this->db->bind('subtitle', $data['subtitle']);
                $this->db->bind('image', $uniqueFileName);
                $this->db->bind('content', $data['content']);
                $this->db->execute();
                return $this->db->rowCount();
            }
        }
    }

    public function deleteBlogData($id)
    {
        $query = "DELETE FROM blog WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);

        $this->db->execute();

        return $this->db->rowCount();
    }
}
