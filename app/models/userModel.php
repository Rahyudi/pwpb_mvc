<?php

class userModel
{
    private $tabel = 'user';
    private $db;
    
    public function __construct()
    {
        $this->db = new Database;
    }
    
    public function getUser()
    {
        $this->db->query('SELECT * FROM ' . $this->tabel);
        return $this->db->resultSet();
    }


    public function login($data)
    {
        $email = $data['email'];
        
        $query = "SELECT * FROM user WHERE email = :email";
        $this->db->query($query);
        $this->db->bind(':email', $email);
        $user = $this->db->single();
        $passwordPost = $data['password'];
        $passwordUser = $user['password'];
        if(password_verify($passwordPost, $passwordUser)){
            $_SESSION['login']=true;
            return 1;
        }
    }

    public function register($data)
    {
        $query = "INSERT INTO user (email, name, password) VALUES(:email, :name, :password)";
        $hashedPassword = password_hash($data['password'], PASSWORD_DEFAULT);
        $this->db->query($query);
        $this->db->bind('email', $data['email']);
        $this->db->bind('name', $data['name']);
        $this->db->bind('password', $hashedPassword);
        $this->db->execute();
        return $this->db->rowCount();
    }
    
}
