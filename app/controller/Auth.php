<?php

class Auth extends Controller
{

    public function __construct()
    {
        if (isset($_SESSION['login']) == true) {
            header('location: ' . BASEURLBLOG . '/home');
        }
    }
     
    public function login()
    {
        $this->view('login', 'auth/login');
    }

    public function register()
    {
        $this->view('register', 'auth/register');
    }

    public function tryLogin()
    {
        if ($this->model('userModel')->login($_POST) > 0) {
            Flasher::setFlash('New user', 'added', 'success');
            header('Location:' . BASEURLBLOG . '/home');
            exit;
        } else {
            Flasher::setFlash('Failed', 'added', 'danger');
            header('Location:' . BASEURLBLOG . '/auth/login');
            exit;
        }
    }

    public function prosesRegister()
    {
        if ($this->model('userModel')->register($_POST) > 0) {
            Flasher::setFlash('New user', 'added', 'success');
            header('Location:' . BASEURLBLOG . '/auth/login');
            exit;
        } else {
            Flasher::setFlash('Failed', 'added', 'danger');
            header('Location:' . BASEURLBLOG . '/auth/register');
            exit;
        }
    }

    public function logout()
    {
        session_destroy();
        header('location: ' . BASEURLBLOG . '/auth/login');
        exit;
    }
}
