<?php

class Home extends Controller
{
    public function __construct()
    {
        if (!$_SESSION['login']) {
            header('location: ' . BASEURLBLOG . '/auth/login');
        }
    }

    public function index()
    {
        $data['dataCreator'] = $this->model('userModel')->getUser();
        $this->view('Home', 'home/index', $data);
    }

    public function create()
    {
        $this->view('Create', 'home/create');
    }

    // public function coba()
    // {
    //     // var_dump($_POST);

    //     if ($this->model('userModel')->login($_POST) > 0) {
    //         Flasher::setFlash('New user', 'added', 'success');
    //         header('Location:' . BASEURLBLOG . '/home');
    //         exit;
    //     } else {
    //         Flasher::setFlash('Failed', 'added', 'danger');
    //         header('Location:' . BASEURLBLOG . '/auth/login');
    //         exit;
    //     }
    // }

    // public function prosesLoginParahCuy()
    // {
    //     var_dump($_POST);
    //     if ($this->model('userModel')->login($_POST) > 0) {
    //         Flasher::setFlash('New user', 'added', 'success');
    //         header('Location:' . BASEURLBLOG . '/home');
    //         exit;
    //     } else {
    //         Flasher::setFlash('Failed', 'added', 'danger');
    //         header('Location:' . BASEURLBLOG . '/auth/login');
    //         exit;
    //     }
    // }
}
