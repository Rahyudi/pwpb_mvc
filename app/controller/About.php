<?php

class About extends Controller 
{

    public function __construct()
    {
        if (!$_SESSION['login']) {
            header('location: ' . BASEURLBLOG . '/auth/login');
        }
    }

    public function index() {
        $data['dataCreator'] = [
            [
                'name' => 'Gde Ngurah Yudiantara',
                'nisn' => '02985',
                'email' => 'gdengurahyudiantara@gmail.com',
                'jurusan' => 'Rekayasa Perangkat Lunak',
                'deskripsi' => 'This "Belajar PHP OOP" Website is created by Ngurah as learning media for PHP OOP.'
            ]
        ];;
        $this->view('Home','about/index',$data);
    }
    
}