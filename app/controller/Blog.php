<?php

class Blog extends Controller 
{

    public function __construct()
    {
        if (!$_SESSION['login']) {
            header('location: ' . BASEURLBLOG . '/auth/login');
        }
    }

    public function index() {
        $data['blog'] = $this->model('blogModel')->getAllBlog();
        $this->view('Home','blog/index',$data);
    }
    public function detail($id) {
        if (!$_SESSION['login']) {
            header('location: ' . BASEURLBLOG . '/auth/login');
        }
        $data['blog'] = $this->model('blogModel')->getAllBlogById($id);
        $this->view('Home','blog/detail',$data);
    }

    // create blog
    public function createBlog(){
        
        if($this->model('blogModel')->newBlog($_POST) > 0){
            Flasher::setFlash('New Blog', 'added', 'success');
            header('Location:' . BASEURLBLOG . '/blog');
            exit;
        } else {
            Flasher::setFlash('Failed', 'added', 'danger');
            header('Location:' . BASEURLBLOG . '/blog');
            exit;
        }
    }
    
    public function deleteBlog($id){
        if($this->model('blogModel')->deleteBlogData($id) > 0){
            Flasher::setFlash('Blog deleted', 'deleted', 'success');
            header('Location:' . BASEURLBLOG . '/blog');
            exit;
        } else {
            Flasher::setFlash('Failed', 'deleted', 'danger');
            header('Location:' . BASEURLBLOG . '/blog');
            exit;
        }
    }
    
}   