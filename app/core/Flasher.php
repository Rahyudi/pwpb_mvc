<?php

class Flasher
{
    public static function setFlash($msg, $act, $typ)
    {
        $_SESSION['flash'] = [
            'msg' => $msg,
            'act' => $act,
            'typ' => $typ
        ];
    }

    public static function flash()
    {
        if (isset($_SESSION['flash'])) {
            echo '<div class="alert alert-' . $_SESSION['flash']['typ'] . ' alert-dismissible fade show" role="alert">
            Artikel <strong>' . $_SESSION['flash']['msg'] . '</strong> ' . $_SESSION['flash']['act'] . '
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"></span>
            </button>
            </div>';
            
            unset($_SESSION['flash']);
        }
    }
}
