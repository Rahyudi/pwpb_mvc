<?php

class App
{
    protected   $controller = 'home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
    {
        $url = $this->parseURL();

        // var_dump($url);
        if (isset($url[0])) {
            if (file_exists('../app/controller/' . $url[0] . '.php')) {
                $this->controller = $url[0];
                unset($url[0]);
            }
        }

        require_once "../app/controller/$this->controller.php";
        $this->controller = new $this->controller;

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        if (!empty($url)) {
            $this->params = array_values($url);
        }

        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function parseURL()
    {
        $url = $_SERVER['REQUEST_URI'];

        // Remove query string from the URL
        $url = strtok($url, '?');

        // Remove leading and trailing slashes
        $url = trim($url, '/');

        // Split the URL into an array using slashes
        $urlParts = explode("/", $url);

        // Get the last part of the URL, which is the filename
        $filename = end($urlParts);

        // Extract directory and basename using pathinfo
        $file_info = pathinfo($filename);
        // var_dump($file_info);
        array_shift($urlParts);
        // You can also return the URL parts as an array if needed
        return $urlParts;
    }
}
