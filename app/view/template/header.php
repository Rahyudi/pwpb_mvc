<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/custom.css">
</head>

<body>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-body ">
        <div class="container">
            <a class="navbar-brand " href="/php-mvc/">PHP MVC</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/php-mvc/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/php-mvc/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/php-mvc/blog">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASEURLBLOG; ?>/auth/logout" onclick="return confirm('yakin logout?')">logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>