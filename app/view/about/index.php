<div class="about-container p-5 rounded">
    <div class="container-fluid d-flex justify-content-center about-header" style="height: 200px;">
        <img src="<?= BASEURL; ?> /img/ryu.jpg" class="position-absolute rounded-circle object-fit-cover " alt=""  style="top: 35%; width: 16vw; height: 16vw; border: solid white 1vw;">
    </div>
    <div class="container-fluid d-flex about-content bg-body-tertiary">
        <div class="" style="margin-top: 10%;">
            <?php foreach ($data['dataCreator'] as $mydata) : ?>
                <h1>
                    <?= $mydata['deskripsi']; ?>
                </h1>
                <div class="d-flex gap-5 about-footer">
                    <h4>
                        <?= $mydata['nisn']; ?>
                    </h4>
                    <h4>
                        <?= $mydata['email']; ?>
                    </h4>
                    <h4>
                        <?= $mydata['jurusan']; ?>
                    </h4>
                </div>

            <?php endforeach; ?>
        </div>
    </div>
</div>