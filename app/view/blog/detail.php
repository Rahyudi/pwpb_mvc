<div class="details">
    <div class="container mt-5 content">
        <img src="<?= BASEURL; ?>/img/<?= $data['blog']['image']; ?> " alt="" width="100%" height="300px" style="object-fit: cover;">
        <h1><?= $data['blog']['title']; ?></h1>
        <h2><?= $data['blog']['subtitle']; ?></h2>
        <p style="font-size: 1.2vw;"><?= $data['blog']['content']; ?></p>
        <div class="d-flex">
            <a class="btn btn-danger w-100" href="<?= BASEURLBLOG; ?>/blog/deleteBlog/<?= $data['blog']['id']; ?>" onclick="return confirm('delete this blog?')">X</a>
        </div>
    </div>
</div>