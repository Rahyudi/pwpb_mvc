<div class="">
    <div class="w-100">
        <?php (Flasher::flash()); ?>
    </div>
</div>
<div class="container-fluid p-2 d-flex flex-row  ">
    <div class="mt-1 blog-link container-fluid  ">
        <div class="container d-flex flex-column gap-5">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Create new blog
            </button>

            <!-- Modal -->
            <div class="modal fade " id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">New Blog</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="<?= BASEURLBLOG; ?>/blog/createBlog" method="post" enctype="multipart/form-data">
                                <div class="mb-3">
                                    <label for="title" class="form-label">Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="Blog title">
                                </div>
                                <div class="mb-3">
                                    <label for="subtitle" class="form-label">Subtitle</label>
                                    <input type="text" class="form-control" name="subtitle" placeholder="Subtitle">
                                </div>
                                <div class="mb-3 ">
                                    <img src="" id="imagePreview" alt="" width="300px" height="300px" style="object-fit: cover;">
                                    <input type="file" class="form-control" name="image" placeholder="Subtitle" id="imageInput">
                                </div>
                                <div class="mb-3">
                                    <label for="content" class="form-label">Content</label>
                                    <textarea class="form-control" name="content" rows="3"></textarea>
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php foreach ($data['blog'] as $mydata) : ?>
                <div class="link">
                    <a href="<?= BASEURLBLOG; ?>/blog/detail/<?= $mydata['id']; ?>">
                        <img src="<?= BASEURL; ?>/img/<?= $mydata['image']; ?>" alt="" width="100%" style="object-fit: cover; height: 400px;">
                        <h1>
                            <?= $mydata['title']; ?>
                        </h1>
                        <h2>
                            <?= $mydata['subtitle']; ?>
                        </h2>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<script>
    const imageInput = document.getElementById('imageInput');
    const imagePreview = document.getElementById('imagePreview');

    imageInput.addEventListener('change', function() {
        if (this.files && this.files[0]) {
            const reader = new FileReader();

            reader.onload = function(e) {
                imagePreview.src = e.target.result;
                imagePreview.style.display = 'block';
            };

            reader.readAsDataURL(this.files[0]);
        }
    });
</script>